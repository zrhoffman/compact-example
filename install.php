#!/usr/bin/env php
<?php

chdir(__DIR__);

$project = 'laravel';
if (!file_exists($project)) {
    $composer = 'composer.phar';
    if (!file_exists($composer)) {
        $installer = 'composer.php';
        file_put_contents($installer, file_get_contents('https://getcomposer.org/installer'));
        shell_exec('php ' . $installer);
        unlink($installer);
    }

    shell_exec(__DIR__ . DIRECTORY_SEPARATOR . $composer . ' create-project ' . $project . '/' . $project);

    $route = 'web.php';
    $view = 'index.blade.php';
    copy($route, implode(DIRECTORY_SEPARATOR, [$project, 'routes', $route]));
    copy($view, implode(DIRECTORY_SEPARATOR, [$project, 'resources', 'views', $view]));
}


echo 'Project is now running at http://localhost:8000' . PHP_EOL;
shell_exec('php -S localhost:8000 -t ' . $project . DIRECTORY_SEPARATOR . 'public');
