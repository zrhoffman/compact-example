<!doctype html>
<html lang="{{ app()->getLocale() }}">
<body style="text-align: center">
<p>{{ $base }}<sup>{{ $base }} + 1</sup> ^ 0b1100 + {{ $base }}
    = {{ $power }} ^ 0b1100 + {{ $base }}
    = {{ $xor }} + {{ $base }}
    = {{ $sum }}</p>
</body>
