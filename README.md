### How to use

Run the installer. This will

* download the composer installer from https://getcomposer.org/installer, run
  it, and remove the installer

* create a new laravel project

* copy `web.php` and `index.blade.php` into their respective places in the
  project

* run the PHP development server at http://localhost:8000

* Notify you that the web server is running

It will skip the first 3 steps the laravel already exists. Once the process
completes, visit http://localhost:8000.
