<?php

Route::get('', function () {
    $base = 2;
    $power = pow($base, $base + 1);
    $xor = $power ^ 0b1100;
    $sum = $xor + $base;

    return view('index', compact([
        'power',
        'sum',
        'xor',
        'base',
    ]));
});
